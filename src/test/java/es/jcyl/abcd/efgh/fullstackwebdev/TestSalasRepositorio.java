package es.jcyl.abcd.efgh.fullstackwebdev;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.SalaEntidad;
import es.jcyl.abcd.efgh.persistencia.repositorios.SalasRepositorio;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FullstackwebdevApplication.class)
public class TestSalasRepositorio {

	@Autowired
	private SalasRepositorio repo;

	@Test
	public void testTodasSalas() throws Exception {
		List<SalaEntidad> salas = (List<SalaEntidad>) repo.findAll();
		assertNotNull(salas);
		assertFalse(salas.isEmpty());
	}

	@Test
	public void testBusquedaPorNombreEdificio() {
		List<SalaEntidad> salas = repo.buscarPorNombreEdificio("eum 1");
		assertNotNull(salas);
		assertFalse(salas.isEmpty());
	}

	@Test
	public void testBusquedaPorCapacidadEntre() {
		List<SalaEntidad> salas = repo.buscarPorCapacidad(12, 100);
		assertNotNull(salas);
		assertFalse(salas.isEmpty());
	}

	@Test
	public void testBusquedaPorTipoSala() {
		List<SalaEntidad> salas = repo.buscarPorTipoSala("audit");
		assertNotNull(salas);
		assertFalse(salas.isEmpty());
	}
}