package es.jcyl.abcd.efgh.fullstackwebdev;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.EdificioEntidad;
import es.jcyl.abcd.efgh.persistencia.entidades.TipoVia;
import es.jcyl.abcd.efgh.persistencia.repositorios.EdificiosRepositorio;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FullstackwebdevApplication.class)
public class TestEdificiosRepositorio {

	@Autowired
	private EdificiosRepositorio repo;

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void testTodosEdificios() throws Exception {
		List<EdificioEntidad> edificios = (List<EdificioEntidad>) repo.findAll();
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}

	@Test
	public void testBusquedaPorProvincia() throws Exception {
		List<EdificioEntidad> edificios = repo.buscarPorProvincia("vall");
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}

	@Test
	public void testBusquedaPorVia() throws Exception {
		List<EdificioEntidad> edificios = repo.buscarPorVia(TipoVia.CALLE, "santiago");
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}

	@Test
	public void testBusquedaPorNombre() throws Exception {
		List<EdificioEntidad> edificios = repo.buscarPorNombre("hospital");
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}

	@Test
	public void testBusquedaEdificiosSinSalas() {
		List<EdificioEntidad> edificios = repo.buscarEdificiosSinSalas();
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}

	@Test
	public void testBuscarPorSalasConCapacidad() {
		List<EdificioEntidad> edificios = repo.buscarEdificiosConSalasYConCapacidad(100);
		assertNotNull(edificios);
		assertTrue(edificios.size() > 0);
	}
}