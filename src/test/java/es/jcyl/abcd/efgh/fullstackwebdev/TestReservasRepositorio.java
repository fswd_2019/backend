package es.jcyl.abcd.efgh.fullstackwebdev;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.ReservaEntidad;
import es.jcyl.abcd.efgh.persistencia.entidades.SalaEntidad;
import es.jcyl.abcd.efgh.persistencia.repositorios.ReservasRepositorio;
import es.jcyl.abcd.efgh.persistencia.repositorios.SalasRepositorio;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FullstackwebdevApplication.class)
public class TestReservasRepositorio {
	
	@Autowired
	private ReservasRepositorio repo;

	@Autowired
	private SalasRepositorio repoSalas;

	@Test
	public void testTodasReservas() throws Exception {
		List<ReservaEntidad> reservas = (List<ReservaEntidad>) repo.findAll();
		assertNotNull(reservas);
		assertFalse(reservas.isEmpty());
	}

	@Test
	public void testDisponibilidadPorSalaFecha() throws Exception {
		SalaEntidad sala = repoSalas.findById(1).get();
		Date fecha = new SimpleDateFormat("dd/MM/yyyy").parse("09/01/2019");
		assertTrue(repo.existeReserva(sala, fecha));
	}
}