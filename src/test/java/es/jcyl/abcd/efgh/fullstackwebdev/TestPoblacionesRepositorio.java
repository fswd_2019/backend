package es.jcyl.abcd.efgh.fullstackwebdev;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.PoblacionEntidad;
import es.jcyl.abcd.efgh.persistencia.entidades.ProvinciaEntidad;
import es.jcyl.abcd.efgh.persistencia.repositorios.PoblacionesRepositorio;
import es.jcyl.abcd.efgh.persistencia.repositorios.ProvinciasRepositorio;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FullstackwebdevApplication.class)
public class TestPoblacionesRepositorio {

	@Autowired
	private PoblacionesRepositorio repoPob;

	@Autowired
	private ProvinciasRepositorio repoProv;

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void testTodasPoblaciones() throws Exception {
		List<PoblacionEntidad> poblaciones = (List<PoblacionEntidad>) repoPob.findAll();
		assertNotNull(poblaciones);
		assertFalse(poblaciones.isEmpty());
	}

	@Test
	public void testBusquedaPorProvincia() throws Exception {
		List<PoblacionEntidad> poblaciones = repoPob.buscarLas5PrimerasPorProvincia("vall");
		assertNotNull(poblaciones);
		assertEquals(poblaciones.size(), 5);
	}

	@Test
	public void testBusquedaPorPoblacion() throws Exception {

		Optional<ProvinciaEntidad> prov = repoProv.buscarPorId(40); // busca valladolid
		assertTrue(prov.isPresent());

		Sort sort = new Sort(Direction.DESC, "poblacion");
		PageRequest pr = PageRequest.of(0, 5, sort);
		Page<PoblacionEntidad> page = repoPob.buscarPorProvinciaEmpiezaPorNombrePaginado(prov.get(), "val", pr);

		assertNotNull(page);
		assertEquals(page.getSize(), 5);
		assertEquals(page.getTotalElements(), 8);
		assertEquals(page.getContent().get(0).getPoblacion(), "Valverde de Campos");
	}
}