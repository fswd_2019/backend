package es.jcyl.abcd.efgh.persistencia.repositorios;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import es.jcyl.abcd.efgh.persistencia.entidades.*;


@Repository
public interface PoblacionesRepositorio extends SoloLecturaRepositorio <PoblacionEntidad, Integer> {
	
	default Optional<PoblacionEntidad> buscarPorId ( Integer id ) {
		return  findById (id);
	}
	
	default List<PoblacionEntidad> buscarLas5PrimerasPorProvincia ( String prov) {
		return findTop5ByProvinciaProvinciaStartingWithIgnoreCaseOrderByPoblacionAsc (prov);
	}
	
    List<PoblacionEntidad> findTop5ByProvinciaProvinciaStartingWithIgnoreCaseOrderByPoblacionAsc (String prov);
	
    
    
    default List<PoblacionEntidad> buscarLas5PrimerasPorProvinciaPorNombre ( ProvinciaEntidad prov , String nombre) {
    	return findTop5ByProvinciaAndPoblacionStartingWithIgnoreCase (prov, nombre);
    }
    
    List<PoblacionEntidad> findTop5ByProvinciaAndPoblacionStartingWithIgnoreCase (ProvinciaEntidad p, String pop);
	
    
    default Page<PoblacionEntidad> buscarPorProvinciaEmpiezaPorNombrePaginado (ProvinciaEntidad prov, String nombre, Pageable pagina) {
    	return findByProvinciaAndPoblacionStartingWithIgnoreCase (prov, nombre , pagina);
    }
    
	Page<PoblacionEntidad> findByProvinciaAndPoblacionStartingWithIgnoreCase (ProvinciaEntidad p, String pop, Pageable pagina);

}


