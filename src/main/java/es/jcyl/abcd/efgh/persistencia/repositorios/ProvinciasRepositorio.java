package es.jcyl.abcd.efgh.persistencia.repositorios;

import java.util.List;
import java.util.Optional;

import es.jcyl.abcd.efgh.persistencia.entidades.ProvinciaEntidad;

public interface ProvinciasRepositorio extends SoloLecturaRepositorio<ProvinciaEntidad, Integer>{
	
	default Optional<ProvinciaEntidad> buscarPorId ( Integer id ) {
		return  findById (id);
	}
	
	default List<ProvinciaEntidad> buscarPorNombre (String prov) {
		return findByProvinciaStartingWithIgnoreCase (prov);
	}

	List<ProvinciaEntidad> findByProvinciaStartingWithIgnoreCase(String prov); 

}
