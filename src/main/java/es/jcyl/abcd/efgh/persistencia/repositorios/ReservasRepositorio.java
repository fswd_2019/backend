package es.jcyl.abcd.efgh.persistencia.repositorios;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.jcyl.abcd.efgh.persistencia.entidades.ReservaEntidad;
import es.jcyl.abcd.efgh.persistencia.entidades.SalaEntidad;

@Repository
public interface ReservasRepositorio extends JpaRepository<ReservaEntidad, Integer> {
	
	
	/*
	@Query("select count(r) > 0 "
		+ "   from ReservaEntidad r "
		+ "  where r.fechaReserva = :fecha")
	public boolean existeReserva ( @Param("sala") SalaEntidad sala , @Param("fecha")Date fecha);
	
	
	*/
	default boolean existeReserva ( SalaEntidad sala , Date fecha ) {
		return existsByFechaReservaAndSala (  fecha , sala);
	}
	
	public boolean existsByFechaReservaAndSala (  @Param("fecha")Date fecha,  @Param("sala") SalaEntidad sala );

}
