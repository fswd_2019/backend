package es.jcyl.abcd.efgh.persistencia.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.jcyl.abcd.efgh.persistencia.entidades.SalaEntidad;

@Repository
public interface SalasRepositorio extends JpaRepository <SalaEntidad, Integer> {
	
	default List<SalaEntidad> buscarPorNombreEdificio ( String nombre ) {
		
		return findByEdificioNombreContainsIgnoreCase (nombre);
	}

	List<SalaEntidad> findByEdificioNombreContainsIgnoreCase(String nombre);
	
	
	default List<SalaEntidad> buscarPorCapacidad ( int min, int max) {
		return findByCapacidadBetween (min,max);
	}

	List<SalaEntidad> findByCapacidadBetween(int min, int max);
	
	
	default List<SalaEntidad> buscarPorTipoSala ( String tipo ) {
		return findByTipoSalaTipoContainsIgnoreCase ( tipo );
	}
	
	List<SalaEntidad> findByTipoSalaTipoContainsIgnoreCase( String tipo);


}
