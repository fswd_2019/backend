package es.jcyl.abcd.efgh.persistencia.repositorios;

import org.springframework.stereotype.Repository;

import es.jcyl.abcd.efgh.persistencia.entidades.TipoSalaEntidad;

@Repository
public interface TipoSalaRepositorio extends SoloLecturaRepositorio <TipoSalaEntidad, Integer> {

}
